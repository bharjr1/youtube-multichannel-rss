Written by Ravi Bharj. Live Demo at https://youtube-multichannel-rss.appspot.com/

This webapp creates a single RSS feed for multiple YouTube channels, which can then be used as a live bookmark.

Clone into the app engine directory, then run the app server with dev_appserver.py

Typical commands:

	$cd /path/to/google_appengine/
	$git clone https://gitlab.com/bharjr1/youtube-multichannel-rss.git
	$python dev_appserver.py --host localhost --port 8080 youtube-multichannel-rss/

ToDo:

	1) Feature: Create favicon
	2) Security: Sanitise description
	3) Security: Add autoescape to index.html
