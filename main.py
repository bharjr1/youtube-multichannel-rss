#!/usr/bin/env python
import os
import webapp2
import jinja2
import urllib
import urllib2
import xml.etree.ElementTree as ET
import datetime
from HTMLParser import HTMLParser

#YT API KEY
#AIzaSyA5kejQiD4AtSpjuqLLQ-v0j2qJYzLnvVM

baseUrl = 'https://www.youtube.com/feeds/videos.xml'

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
        if tag == 'meta':
            for name,value in attrs:
                if name=='content' and value[0:19] == 'vnd.youtube://user/':
                    self.RSLT = value[19:]

def sanit(text):
        s = str()
        for i in text:
                i = ord(i)
                if (i>=48 and i<=57) or (i>=65 and i<= 90) or (i>=97 and i<=122) or (i==44) or (i==46):
                        s += chr(i)
                elif (i==195) or (i==129) or (i==137) or (i==141) or (i==145) or (i==147) or (i==154) or (i==169) or (i==173) or (i==177) or (i==179) or (i==186):
                        s += chr(i)
        return s

def sanit2(text):
    s = str()
    for i in text:
        if ord(i) <= 122:
            s += i
    return s

def getTime(t):
    return datetime.datetime( int(t[0:4]), int(t[5:7]), int(t[8:10]), int(t[11:13]), int(t[14:16]), int(t[17:19]) )

def getVids(root,newTable):
    for entry in root.findall('{http://www.w3.org/2005/Atom}entry'):
        v = entry.find('{http://www.youtube.com/xml/schemas/2015}videoId').text
        t = getTime(entry.find('{http://www.w3.org/2005/Atom}published').text)
        T = entry.find('{http://www.w3.org/2005/Atom}title').text
        a = entry.find('{http://www.w3.org/2005/Atom}author').find('{http://www.w3.org/2005/Atom}name').text
        d = entry.find('{http://search.yahoo.com/mrss/}group').find('{http://search.yahoo.com/mrss/}description').text
        (videoId, date, title, author, description ) = (v, t, T, a, d)
        newTable.append( (videoId, date, title, author, description) )

parser = MyHTMLParser()

class MainHandler(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('html/index.html')
        self.response.write(template.render())

class rssHandler(webapp2.RequestHandler):
    def get(self):
        uInput = self.request.get('channels')
        uInput = uInput.replace(chr(32),'')
        newTable = []
        channel = uInput.split(',')

        for i in range(len(channel)):
            channel[i] = sanit(channel[i])
            page = 'https://www.youtube.com/user/'+channel[i]+'/videos'
            try:
                f = urllib2.urlopen(page)
                html = sanit2( f.read() )
                parser.feed(html)
                f = urllib2.urlopen(baseUrl+'?channel_id='+parser.RSLT)
                root = ET.fromstring( f.read() )
                getVids(root, newTable)
            except:
                continue

        fullTitle = str()
        for i in channel:
            fullTitle += i + ', '
        fullTitle = fullTitle[:-2]
        newTable = sorted(newTable, key=lambda r: r[1], reverse=True)

        rootNode = ET.Element('feed')
        rootNode.attrib = { 'xmlns':'http://www.w3.org/2005/Atom' }
#       rootNode.attrib = { 'xmlns':'http://www.w3.org/2005/Atom', 'xmlns:georss':'http://www.georss.org/georss', 'xmlns:gml':'http://www.opengis.net/gml' }

        a = ET.SubElement(rootNode,'title')
        b = ET.SubElement(rootNode,'link')
        c = ET.SubElement(rootNode,'id')
        d = ET.SubElement(rootNode,'updated')
        e = ET.SubElement(rootNode,'generator')

        a.text = fullTitle
        b.attrib = {'href':'host', 'rel':'self'}
        c.text = 'http://localhost/'
        d.text = datetime.datetime.now().isoformat()
        e.text = 'host'
        e.attrib = { 'uri':'http://localhost/' }
        for i in newTable:
            this = ET.SubElement(rootNode,'entry')
            a = ET.SubElement(this,'link')
            b = ET.SubElement(this,'updated' )
            c = ET.SubElement(this,'title')
            d = ET.SubElement(this,'summary')
            e = ET.SubElement(this,'id')
            f = ET.SubElement(this,'author')
            g = ET.SubElement(f,'name')
            a.attrib = {'href':'https://www.youtube.com/watch?v='+i[0]}
            b.text = i[1].isoformat()
            c.text = i[2]
            d.text = i[4]
            e.text = i[0]
            g.name = i[3]
        self.response.write( ET.tostring(rootNode, encoding="UTF-8", method="xml") )

#        template_values = { 'theGun':channel }
#        template = JINJA_ENVIRONMENT.get_template('html/rsspage.html')
#        self.response.write(template.render(template_values))

app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/rss', rssHandler)
], debug=True)
